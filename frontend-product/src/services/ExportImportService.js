import http from "./http-common";

const getAll = () => {
    return http.post("/importar");
};
const create = data => {
    const { categoria, names } = data;
    var products = JSON.parse(names);
    var json = JSON.stringify({ products });
    return http.post(`/${categoria}/exportar`, json);
};
const ProdutoService = {
    getAll,
    create,
};
export default ProdutoService;