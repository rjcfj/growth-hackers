import http from "./http-common";

const getAll = () => {
    return http.get("/categorias");
};
const get = id => {
    return http.get(`/categorias/${id}`);
};
const create = data => {
    return http.post("/categorias", data);
};
const update = (id, data) => {
    return http.put(`/categorias/${id}`, data);
};
const remove = id => {
    return http.delete(`/categorias/${id}`);
};
const CategoriasService = {
    getAll,
    get,
    create,
    update,
    remove
};
export default CategoriasService;