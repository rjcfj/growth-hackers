import React, { useState, useEffect } from "react";
import ProdutoDataService from "../../services/ProdutoService";
import { Link } from "react-router-dom";
const List = () => {
    const [produtos, setProdutos] = useState([]);
    const [currentProduto, setCurrentProduto] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    useEffect(() => {
        retrieveProdutos();
    }, []);
    const retrieveProdutos = () => {
        ProdutoDataService.getAll()
            .then(response => {
                setProdutos(response.data.data);
                console.log(response.data.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    const setActiveProduto = (produto, index) => {
        setCurrentProduto(produto);
        setCurrentIndex(index);
    };
    return (
        <div className="list row">
            <div className="col-md-8">
                <div className="input-group mb-3">
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button">
                            <Link to={"/produtos/add"}>Adicionar</Link>
                        </button>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <h4>Lista de Produtos</h4>
                <ul className="list-group">
                    {produtos && produtos.map((produto, index) => (
                        <li
                            className={
                                "list-group-item " + (index === currentIndex ? "active" : "")
                            }
                            onClick={() => setActiveProduto(produto, index)}
                            key={index}
                        >
                            {produto.name} - {produto.categoria}
                        </li>
                    ))}
                </ul>
            </div>
            <div className="col-md-6">
                {currentProduto ? (
                    <div>
                        <h4>Produto</h4>
                        <div>
                            <label><strong>Nome:</strong></label>{" "}
                            {currentProduto.name}
                        </div>
                        <div>
                            <label><strong>Categoria:</strong></label>{" "}
                            {currentProduto.categoria}
                        </div>
                        <Link to={"/produtos/" + currentProduto.id} className="btn btn-warning text-dark" > Editar </Link>
                    </div>
                ) : (
                    <div><br /><p>Clique em um produto...</p></div>
                )}
            </div>
        </div>
    );
};
export default List;