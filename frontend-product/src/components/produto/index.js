import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import ProdutoDataService from "../../services/ProdutoService";
import CategoriaDataService from "../../services/CategoriasService";
const Produto = props => {
    const { id } = useParams();
    let navigate = useNavigate();
    const initialTutorialState = { id: null, name: "", categoria_id: "", };
    const [currentProduto, setCurrentProduto] = useState(initialTutorialState);
    const [categorias, setCategorias] = useState([]);
    const [message, setMessage] = useState("");
    const getProduto = id => {
        ProdutoDataService.get(id)
            .then(response => {
                setCurrentProduto(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    useEffect(() => {
        if (id)
            getProduto(id);
            getOptions();
    }, [id]);
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentProduto({ ...currentProduto, [name]: value });
    };
    const updateProduto = () => {
        ProdutoDataService.update(currentProduto.id, currentProduto)
            .then(response => {
                console.log(response.data);
                setMessage("O Produto foi atualizado com sucesso!");
            })
            .catch(e => {
                console.log(e);
            });
    };
    const deleteProduto = () => {
        ProdutoDataService.remove(currentProduto.id)
            .then(response => {
                console.log(response.data);
                navigate("/produtos");
            })
            .catch(e => {
                console.log(e);
            });
    };

    const getOptions = () => {
        CategoriaDataService.getAll()
            .then(response => {

                const data = response.data.data.map(d => (
                    { "value": d.id, "label": d.name }
                ));
                console.log(response.data.data);
                setCategorias(data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    const backProduto = () => {
        navigate("/produtos");
    };
    return (
        <div>
            {currentProduto ? (
                <div className="edit-form">
                    <h4>Produto</h4>
                    <form>
                        <div className="form-group">
                            <label htmlFor="name">Nome</label>
                            <input type="text" className="form-control" id="name" name="name" value={currentProduto.name} onChange={handleInputChange} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="categoria_id">Categoria</label>
                            <select className="form-control" name="categoria_id" id="categoria_id" value={currentProduto.categoria_id} onChange={handleInputChange} required>
                                <option value=""></option>
                                {categorias && categorias.map((categoria, index) => (
                                    <option key={index}  value={categoria.value}>{categoria.label}</option>
                                ))}
                            </select>
                        </div>
                    </form>
                    <p>{message}</p>
                    <button className="btn btn-danger" onClick={deleteProduto}>Excluir</button>
                    <button type="submit" style={{ marginLeft: '15px'}} className="btn btn-success" onClick={updateProduto} >Atualizar</button>
                    <button type="submit" style={{ marginLeft: '15px'}} className="btn btn-primary" onClick={backProduto} >Voltar</button>
                </div>
            ) : (
                <div><br/><p>Clique em um produto...</p></div>
            )}
        </div>
    );
};
export default Produto;