import React, { useState, useEffect } from "react";
import ProdutoDataService from "../../services/ProdutoService";
import CategoriaDataService from "../../services/CategoriasService";
import { useNavigate } from 'react-router-dom';

const Add = () => {

    let navigate = useNavigate();
    const [isShow, setIsShow] = useState(true);
    const initialCategoriasState = { id: null, categoriaName: "", };
    const initialProdutoState = { id: null, name: "", categoria_id: "", };
    const [categoria, setCategoria] = useState(initialCategoriasState);
    const [produto, setProduto] = useState(initialProdutoState);
    const [categorias, setCategorias] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [submittedCategoria, setSubmittedCategoria] = useState(false);

    useEffect(() => {
        getOptions();
    }, []);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setProduto({ ...produto, [name]: value });
    };
    const handleInputChangeCategoria = event => {
        const { name, value } = event.target;
        setCategoria({ ...categoria, [name]: value });
    };
    const saveCategoria = () => {
        var data = {
            name: categoria.categoriaName,
        };
        CategoriaDataService.create(data)
            .then(response => {
                setProduto({
                    categoria_id: response.data.id,
                });
                console.log(response.data);
                setSubmittedCategoria(true);
            }).catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    };

    const saveProduto = () => {
        var data = {
            name: produto.name,
            categoria_id: produto.categoria_id
        };
        console.log(data);
        ProdutoDataService.create(data)
            .then(response => {
                console.log(response);
                setProduto({
                    id: response.data.id,
                    name: response.data.name,
                    categoria_id: response.data.categoria_id,
                });
                setSubmitted(true);
                console.log(response.data);
            }).catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    };
    const newProduto = () => {
        setProduto(initialProdutoState);
        setSubmitted(false);
    };

    const getOptions = () => {
        CategoriaDataService.getAll()
            .then(response => {

                const data = response.data.data.map(d => (
                    { "value": d.id, "label": d.name }
                ));
                console.log(response.data.data);
                setCategorias(data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    const backProduto = () => {
        navigate("/produtos");
    };

    const handleClick = () => {
        setIsShow(!isShow);
    };

    return (
        <div className="submit-form">
            <h1>Produto</h1>
            {submitted ? (
                <div>
                    <h4>O Produto foi salvando com sucesso!</h4>
                    <button className="btn btn-success" onClick={newProduto}>Adicionar</button>
                    <button className="btn btn-danger" style={{ marginLeft: '15px' }} onClick={backProduto}>Voltar</button>
                </div>
            ) : (
                <div>
                    <div cassName="form-group">
                        <label htmlFor="categoria_id">Categoria</label>
                        {isShow ? (
                            <select className="form-control" name="categoria_id" id="categoria_id" onChange={handleInputChange} required>
                                <option value=""></option>
                                {categorias && categorias.map((categoria, index) => (
                                    <option key={index} value={categoria.value}>{categoria.label}</option>
                                ))}
                            </select>
                        ) : (
                            <input
                                type="text"
                                className="form-control"
                                id="categoriaName"
                                required
                                value={categoria.name}
                                onChange={handleInputChangeCategoria}
                                name="categoriaName"
                            />
                        )}
                        {submittedCategoria ? ( <p>A Categoria foi salvando com sucesso!</p> ) : ( <div></div> )}
                        {isShow ? (
                            <button style={{ marginBottom: '15px' }} onClick={handleClick} className="btn btn-success">Adicionar Categoria</button>
                        ) : (
                            <button style={{ marginBottom: '15px' }} onClick={saveCategoria} className="btn btn-success">Salvar Categoria </button>
                        )}
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Nome</label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            required
                            value={produto.name}
                            onChange={handleInputChange}
                            name="name"
                        />
                    </div>
                    <button style={{ marginTop: '15px' }} onClick={saveProduto} className="btn btn-success">Salvar</button>
                </div>
            )}
        </div>
    );
};
export default Add;