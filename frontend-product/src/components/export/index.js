import React, { useState, useEffect } from "react";
import ExportImportService from "../../services/ExportImportService";
import CategoriaDataService from "../../services/CategoriasService";
import { useNavigate } from 'react-router-dom';

const Add = () => {
    let navigate = useNavigate();
    const initialProdutoState = { id: null, name: "", categoria_id: "", };
    const [produto, setProduto] = useState(initialProdutoState);
    const [categorias, setCategorias] = useState([]);
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        getOptions();
    }, []);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setProduto({ ...produto, [name]: value });
    };
    const saveProduto = () => {

        var data = {
            names: produto.name,
            categoria: produto.categoria_id
        };

        ExportImportService.create(data)
            .then(response => {
                if (response.data.message === 'Não existe nome de categoria') {
                    setSubmitted(false);
                    console.log(response.data.message);
                } else {
                    setSubmitted(true);
                    console.log(response);
                }
            })
            .catch(e => {
                console.log(e);
            });
    };
    const newProduto = () => {
        setProduto(initialProdutoState);
        setSubmitted(false);
    };

    const getOptions = () => {
        CategoriaDataService.getAll()
            .then(response => {

                const data = response.data.data.map(d => (
                    { "value": d.id, "label": d.name }
                ));
                console.log(response.data.data);
                setCategorias(data)
            })
            .catch(e => {
                console.log(e);
            });
    };

    const backProduto = () => {
        navigate("/produtos");
    };

    return (
        <div className="submit-form">
            {submitted ? (
                <div>
                    <h4>Os Produtos com uma categoria foi salvando com sucesso!</h4>
                    <button className="btn btn-success" onClick={newProduto}>Adicionar</button>
                    <button className="btn btn-danger" style={{ marginLeft: '15px' }} onClick={backProduto}>Voltar</button>
                </div>
            ) : (
                <div>
                    <div className="form-group">
                        <label htmlFor="categoria_id">Categoria</label>
                        <select className="form-control" name="categoria_id" id="categoria_id" onChange={handleInputChange} required>
                            <option value=""></option>
                            {categorias && categorias.map((categoria, index) => (
                                <option key={index} value={categoria.label}>{categoria.label}</option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Formato JSON</label>
                        <textarea className="form-control" name="name" id="name" onChange={handleInputChange} required rows="10"></textarea>
                    </div>
                    <button style={{ marginTop: '15px' }} onClick={saveProduto} className="btn btn-success">Salvar</button>
                </div>
            )}
        </div>
    );
};
export default Add;