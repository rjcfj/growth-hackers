import React, { useState, useEffect } from "react";
import ExportImportService from "../../services/ExportImportService";
const Index = props => {
    const getData = () => {
        ExportImportService.getAll()
            .then(function (response) {
                console.log(response)
                const fileName = "file";
                const json = JSON.stringify(response);
                const blob = new Blob([json], { type: 'application/json' });
                const href = URL.createObjectURL(blob);
                const link = document.createElement('a');
                link.href = href;
                link.download = fileName + ".json";
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            })
    }
    useEffect(() => {
        getData()
    }, [])
    return (
        <div className="App">
            <h1>Download Automatico</h1>
        </div>
    );
}
export default Index;