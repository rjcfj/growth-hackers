import React, { useState } from "react";
import CategoriaDataService from "../../services/CategoriasService";
import { useNavigate } from 'react-router-dom';

const Add = () => {
    let navigate = useNavigate();
    const initialCategoriasState = {id: null,name: "",};
    const [categoria, setCategoria] = useState(initialCategoriasState);
    const [submitted, setSubmitted] = useState(false);
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCategoria({ ...categoria, [name]: value });
    };
    const saveCategoria = () => {
        var data = {
            name: categoria.name,
        };
        CategoriaDataService.create(data)
            .then(response => {
                setCategoria({
                    id: response.data.id,
                    name: response.data.name,
                });
                setSubmitted(true);
                console.log(response.data);
            }).catch(function (error) {
                if (error.response) {
                    console.log(error.response.data.error);
                }
            });
    };
    const newCategoria = () => {
        setCategoria(initialCategoriasState);
        setSubmitted(false);
    };
    const backCategoria = () => {
        navigate("/categorias");
    };
    return (
        <div className="submit-form">
            <h1>Categoria</h1>
            {submitted ? (
                <div>
                    <h4>A Categoria foi salvando com sucesso!</h4>
                    <button className="btn btn-success" onClick={newCategoria}>Adicionar</button>
                    <button className="btn btn-danger" style={{ marginLeft: '15px'}} onClick={backCategoria}>Voltar</button>
                </div>
            ) : (
                <div>
                    <div className="form-group">
                        <label htmlFor="name">Nome</label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            required
                            value={categoria.name}
                            onChange={handleInputChange}
                            name="name"
                        />
                    </div>
                    <button style={{marginTop: '15px'}} onClick={saveCategoria} className="btn btn-success">Salvar</button>
                </div>
            )}
        </div>
    );
};
export default Add;