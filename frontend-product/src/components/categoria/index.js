import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import CategoriaDataService from "../../services/CategoriasService";
const Categoria = props => {
    const { id } = useParams();
    let navigate = useNavigate();
    const initialTutorialState = { id: null, name: "", };
    const [currentCategoria, setCurrentCategoria] = useState(initialTutorialState);
    const [message, setMessage] = useState("");
    const getCategoria = id => {
        CategoriaDataService.get(id)
            .then(response => {
                setCurrentCategoria(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    useEffect(() => {
        if (id)
            getCategoria(id);
    }, [id]);
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentCategoria({ ...currentCategoria, [name]: value });
    };
    const updatePublished = status => {
        var data = {
            id: currentCategoria.id,
            name: currentCategoria.name,
        };
        CategoriaDataService.update(currentCategoria.id, data)
            .then(response => {
                setCurrentCategoria({ ...currentCategoria });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    const updateCategoria = () => {
        CategoriaDataService.update(currentCategoria.id, currentCategoria)
            .then(response => {
                console.log(response.data);
                setMessage("A Categoria foi atualizado com sucesso!");
            })
            .catch(e => {
                console.log(e);
            });
    };
    const deleteCategoria = () => {
        CategoriaDataService.remove(currentCategoria.id)
            .then(response => {
                console.log(response.data);
                navigate("/categorias");
            })
            .catch(e => {
                console.log(e);
            });
    };
    const backCategoria = () => {
        navigate("/categorias");
    };
    return (
        <div>
            {currentCategoria ? (
                <div className="edit-form">
                    <h4>Categoria</h4>
                    <form>
                        <div className="form-group">
                            <label htmlFor="name">Nome</label>
                            <input type="text" className="form-control" id="name" name="name" value={currentCategoria.name} onChange={handleInputChange} />
                        </div>
                    </form>
                    <p>{message}</p>
                    <button className="btn btn-danger mr-2" onClick={deleteCategoria}>Excluir</button>
                    <button type="submit" style={{ marginLeft: '15px'}} className="btn btn-success" onClick={updateCategoria}>Atualizar</button>
                    <button type="submit" style={{ marginLeft: '15px'}} className="btn btn-primary" onClick={backCategoria} >Voltar</button>
                </div>
            ) : (
                <div><br /><p>Clique em uma categoria...</p></div>
            )}
        </div>
    );
};
export default Categoria;