import React, { useState, useEffect } from "react";
import CategoriaDataService from "../../services/CategoriasService";
import { Link } from "react-router-dom";
const List = () => {
    const [categorias, setCategorias] = useState([]);
    const [currentCategoria, setCurrentCategoria] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(-1);
    useEffect(() => {
        retrieveCategorias();
    }, []);
    const retrieveCategorias = () => {
        CategoriaDataService.getAll()
            .then(response => {
                setCategorias(response.data.data);
                console.log(response.data.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
    const setActiveCategoria = (categoria, index) => {
        setCurrentCategoria(categoria);
        setCurrentIndex(index);
    };
    return (
        <div className="list row">
            <div className="col-md-8">
                <div className="input-group mb-3">
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button">
                            <Link to={"/categorias/add"}>Adicionar</Link>
                        </button>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <h4>Lista de Categorias</h4>
                <ul className="list-group">
                    {categorias && categorias.map((categoria, index) => (
                        <li
                            className={
                                "list-group-item " + (index === currentIndex ? "active" : "")
                            }
                            onClick={() => setActiveCategoria(categoria, index)}
                            key={index}
                        >
                            {categoria.name}
                        </li>
                    ))}
                </ul>
            </div>
            <div className="col-md-6">
                {currentCategoria ? (
                    <div>
                        <h4>Categoria</h4>
                        <div>
                            <label><strong>Nome:</strong></label>{" "}
                            {currentCategoria.name}
                        </div>
                        <Link to={"/categorias/" + currentCategoria.id} className="btn btn-warning text-dark" > Editar </Link>
                    </div>
                ) : (
                    <div><br /><p>Clique em uma Categoria...</p></div>
                )}
            </div>
        </div>
    );
};
export default List;