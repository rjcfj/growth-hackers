import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import AddCategoria from "./components/categoria/add";
import Categoria from "./components/categoria/index";
import CategoriasList from "./components/categoria/list";
import AddProduto from "./components/produto/add";
import Produto from "./components/produto/index";
import ProdutosList from "./components/produto/list";
import Import from "./components/import/index";
import Export from "./components/export/index";

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/" className="navbar-brand">GrowthHackers</a>
        <div className="navbar-nav mr-auto">
        <li className="nav-item">
            <Link to={"/categorias"} className="nav-link">Categorias</Link>
          </li>
          <li className="nav-item">
            <Link to={"/produtos"} className="nav-link">Produtos</Link>
          </li>
          <li className="nav-item">
            <Link to={"/export"} className="nav-link">Exportar</Link>
          </li>
          <li className="nav-item">
            <Link to={"/import"} className="nav-link">Importar</Link>
          </li>
        </div>
      </nav>
      <div className="container mt-3">
        <Routes>
          <Route path="/categorias" element={<CategoriasList />} />
          <Route path="/categorias/add" element={<AddCategoria />} />
          <Route path="/categorias/:id" element={<Categoria />} />
          <Route path="/produtos" element={<ProdutosList />} />
          <Route path="/produtos/add" element={<AddProduto />} />
          <Route path="/produtos/:id" element={<Produto />} />
          <Route path="/export" element={<Export />} />
          <Route path="/import" element={<Import />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
