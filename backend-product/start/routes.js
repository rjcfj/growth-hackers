'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Desafio de Produto - Fullstack' }
})

Route.get('/categorias', 'CategoriaController.list');
Route.post('/categorias', 'CategoriaController.store');
Route.get('/categorias/:id', 'CategoriaController.show');
Route.put('/categorias/:id', 'CategoriaController.update');
Route.delete('/categorias/:id', 'CategoriaController.delete');

Route.get('/produtos', 'ProdutoController.list');
Route.post('/produtos', 'ProdutoController.store');
Route.get('/produtos/:id', 'ProdutoController.show');
Route.put('/produtos/:id', 'ProdutoController.update');
Route.delete('/produtos/:id', 'ProdutoController.delete');

Route.post('/:categoria/exportar', 'ExportImportController.export');
Route.post('/importar', 'ExportImportController.import');
