'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutosSchema extends Schema {
  up() {
    this.create('produtos', (table) => {
      table.increments()
      table.string('name', 200).notNullable().unique()
      table.integer('categoria_id').notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('produtos')
  }
}

module.exports = ProdutosSchema
