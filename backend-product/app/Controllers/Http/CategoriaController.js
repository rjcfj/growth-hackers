'use strict'

const Categoria = use('App/Models/Categoria');

class CategoriaController {

  async list({ params: { page } }) {
    const pageNumber = page || 1;
    const categorias = await Categoria.query().orderBy("id", "desc").paginate(pageNumber, 10)
     return categorias.toJSON();
  }

  async store({ request }) {
    const dataToCreate = request.only(['name']);
    return await Categoria.create(dataToCreate);
  }

  async show({ params }) {
    const { id } = params
    return await Categoria.find(id);
  }

  async update({ params, request }) {
    const categoria = await Categoria.findOrFail(params.id);
    const dataToUpdate = request.only(['name']);
    categoria.merge(dataToUpdate);
    await categoria.save();
    return categoria;
  }

  async delete({ params }) {
    const categoria = await Categoria.findOrFail(params.id);
    await categoria.delete();
    return { message: 'Categoria Deletado!'}
  }
}

module.exports = CategoriaController
