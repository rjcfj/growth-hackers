'use strict'

const Produto = use('App/Models/Produto');

class ProdutoController {

  async list({ params: { page } }) {
    const pageNumber = page || 1;
    const produtos = await Produto.query()
    .select('produtos.id as id', 'produtos.name as name', 'categorias.id as categoria_id' ,'categorias.name as categoria')
    .join('categorias', 'produtos.categoria_id', '=', 'categorias.id')
    .orderBy("id", "desc").paginate(pageNumber, 10);

    return produtos.toJSON();
  }

  async store({ request }) {
    const dataToCreate = request.only(['name', 'categoria_id']);
    return await Produto.create(dataToCreate);
  }

  async show({ params, response }) {
    const { id } = params
    return await Produto.find(id);
  }

  async update({ params, request }) {
    const produto = await Produto.findOrFail(params.id);
    const dataToUpdate = request.only(['name', 'categoria_id']);
    produto.merge(dataToUpdate);
    await produto.save();
    return produto;

  }

  async delete({ params }) {
    const produto = await Produto.findOrFail(params.id);
    await produto.delete();
    return { message: 'Produto Deletado!'}
  }

}

module.exports = ProdutoController
