'use strict'

const Produto = use('App/Models/Produto');
const Categoria = use('App/Models/Categoria');

class ExportImportController {

  async export({ params, request }) {

    try {

      const categoria_id = await Categoria.findByOrFail('name', params.categoria);
      const productList = request.input("products");

      productList.forEach(async (info) => {
        const dataToCreate = {
          name: info.name,
          categoria_id: categoria_id.id
        };
        return await Produto.create(dataToCreate);
      });

      return { message: 'Salvado com sucesso!' };

    } catch (error) {
      return { message: 'Não existe nome de categoria' };
    }

  }

  async import() {

    const groupByKey = (list, key, { omitKey = false }) => list.reduce((hash, { [key]: value, ...rest }) => ({ ...hash, [value]: (hash[value] || []).concat(omitKey ? { ...rest } : { [key]: value, ...rest }) }), {})

    const inputArray = await Produto.query()
      .select('produtos.name as product', 'categorias.name as category')
      .join('categorias', 'produtos.categoria_id', '=', 'categorias.id').fetch();

    return groupByKey(inputArray.toJSON(), 'category', { omitKey: true })
  }
}

module.exports = ExportImportController
