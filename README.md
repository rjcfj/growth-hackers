# growth-hackers

Desafio - GrowthHackers- Back/Front

# Requisitos:
BackEnd
- `AdonisJS`
- `postgres`

FrontEnd
- `ReactJS`

## Como executar:

Antes de executar, certifique-se de atualizar o arquivo .env na raiz do projeto com as variaveis do seu ambiente e atualize o banco de dados com as migrações com o comando: adonis migration:run

## Você pode instalar o pacote do npm.
Se você nao tem o projeto do Adonisjs:

```
npm i --global @adonisjs/cli

```

## Clona os projetos

```
https://gitlab.com/rjcfj/growth-hackers.git -b main

```

## BackEnd

```
cd growth-hackers/backend-product

yarn install

cp .env.example .env

adonis migration:run

yarn dev
```

## BackEnd

```
cd growth-hackers/frontend-product

yarn install

yarn dev
```